import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'wfm-auth',
    templateUrl: './auth.component.html'
})

export class AuthComponent implements OnInit {
    constructor(private roter: Router) {}

    ngOnInit() {
        this.roter.navigate(['/login']);
    }
}