export class AuthService {
    private isAuthenticated = false;

    public login() {
        this.isAuthenticated = true;
    }

    public logout() {
        this.isAuthenticated = true;
        window.localStorage.clear();
    }

    public isLoggedIn(): boolean {
        return this.isAuthenticated;
    }
}